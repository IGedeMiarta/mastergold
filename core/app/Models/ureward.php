<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ureward extends Model
{
    use HasFactory;

    public function rewa()
    {
        return $this->belongsTo(bonus_reward::class, 'reward_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
