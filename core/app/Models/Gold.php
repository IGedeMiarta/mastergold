<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gold extends Model
{
    use HasFactory;
    protected $table ="golds";

    public function prod()
    {
        return $this->belongsTo(Product::class);
    }
    public function cord()
    {
        return $this->hasOne(corder::class);
    }
}
