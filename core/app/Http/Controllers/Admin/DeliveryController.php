<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ExportData;
use App\Http\Controllers\Controller;
use App\Models\Gold;
use App\Models\sendgold;
use App\Models\sgdetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class DeliveryController extends Controller
{
    //
    public function index(Request $request){
        
        
            $items = sendgold::join('users','users.id','=','sendgolds.user_id')->leftjoin('golds','golds.id','=','sendgolds.gold_id')
            ->leftjoin('products','products.id','=','golds.prod_id')
            ->leftjoin('corders','corders.gold_id','=','golds.id')
            ->select('sendgolds.*','sendgolds.id as sgid','sendgolds.status as st','sendgolds.created_at as ct','golds.is_custom','products.name as pname', 'products.weight as pweight','corders.name as cname','users.*')
            ->with('detail')
            ->orderBy('sendgolds.created_at','DESC')
            ->groupBy('sendgolds.id')
            ->get();
            $page_title = 'Gold Delivery';
            $empty_message = "Gold Delivery Request Not Found!";
        
        
        return view('admin.delivery.delivery',compact('page_title','items','empty_message'));
    }
    public function export(Request $request){
        
        if (isset($request->status)) {
            if ($request->status == 'pending') {
                $status = 2;
                $sts = 'pending';
            }else{
                $status = 1;
                $sts = 'success';
            }
            $page_title = 'Gold Delivery';
            $empty_message = "Gold Delivery Request Not Found!";
            $items = sendgold::query()->leftjoin('golds','golds.id','=','sendgolds.gold_id')
            ->leftjoin('products','products.id','=','golds.prod_id')
            ->leftjoin('corders','corders.gold_id','=','golds.id')
            ->join('users','users.id','=','sendgolds.user_id')
            ->select('sendgolds.created_at','sendgolds.trx','users.username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'sendgolds.alamat',db::raw("if(sendgolds.status = 2, 'pending','success')"),'products.name', 'products.weight','corders.name')
            ->where('sendgolds.status',$status)
            ->orderBy('sendgolds.created_at','DESC')
            ;
            $search = '';
        }
        // dd($request->all());

        if (isset($request->search)) {
            $search = $request->search;
            $page_title = 'Gold Delivery';
            $empty_message = "Gold Delivery Request Not Found!";
            $items = sendgold::query()->leftjoin('golds','golds.id','=','sendgolds.gold_id')
            ->leftjoin('products','products.id','=','golds.prod_id')
            ->leftjoin('corders','corders.gold_id','=','golds.id')
            ->join('users','users.id','=','sendgolds.user_id')
            ->select('sendgolds.created_at','sendgolds.trx','users.username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'sendgolds.alamat',db::raw("if(sendgolds.status = 2, 'pending','success')"),'products.name', 'products.weight','corders.name');
            $items = $items->where(function ($user) use ($search) {
                $user->where('users.username', 'like', "%$search%")
                        ->orWhere('users.no_bro', 'like', "%$search%")
                        ->orWhere('sendgolds.trx', 'like', "%$search%");
                        });
            $items = $items->orderBy('sendgolds.created_at','DESC');
            $sts = '';
        }

        if(!isset($request->status) && !isset($request->search)){
            $sts = '';
            $search = '';
            $items = sendgold::query()->leftjoin('golds','golds.id','=','sendgolds.gold_id')
            ->leftjoin('products','products.id','=','golds.prod_id')
            ->leftjoin('corders','corders.gold_id','=','golds.id')
            ->join('users','users.id','=','sendgolds.user_id')
            ->select('sendgolds.created_at','sendgolds.trx','users.username',db::raw("CONCAT('+',' ',mobile) AS no_hp"),'sendgolds.alamat',db::raw("if(sendgolds.status = 2, 'pending','success')"),'products.name', 'products.weight','corders.name')
            ->orderBy('sendgolds.created_at','DESC')
            ;
            $page_title = 'Gold Delivery';
            $empty_message = "Gold Delivery Request Not Found!";
        }

        return Excel::download(
            new ExportData($items), 'delivery.xlsx');
    }

    public function delivery(Request $request){
        // dd($request->all());
        $sg = sendgold::where('id',$request->id)->first();
        $sg->no_resi = $request->no_resi;
        $sg->status = 1;
        $sg->save();

        $sgdet = sgdetail::where('sg_id',$sg->id)->get();

        // $gold = Gold::where('id',$sg->gold_id)->first();
        // $gold->qty -= $sg->qty;
        // $gold->save();
        // foreach ($sgdet as $item) {

        //     $gold = Gold::where('id',$item->gold_id)->first();
        //     $gold->qty -= $item->qty;
        //     $gold->save();
        // }

        adminlog(Auth::guard('admin')->user()->id,'Input Resi Number '. $sg->no_resi .' in trx : '. $sg->trx);
        // dd($sgdet);

        $notify[] = ['success', 'Gold Deliver successfully'];
        return back()->withNotify($notify);
    }
}
