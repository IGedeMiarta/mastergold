@extends('admin.layouts.app')

@section('panel')
<div class="row">
    <div class="col-lg-12">
        <div class="card b-radius--10 ">
            <div class="card-body p-0">
                <div class=" table-responsive p-2">
                    <input hidden type="text" id="min2" name="min2">
                    <input hidden type="text" id="max2" name="max2">
                        <table class="table table--light style--two" id="myTable" >
                        <thead>
                            <tr>
                                {{-- <th scope="col">@lang('SL')</th> --}}
                                <th scope="col">@lang('Date')</th>
                                <th scope="col">@lang('Time')</th>
                                <th scope="col">@lang('Delivery ID')</th>
                                <th scope="col">@lang('User')</th>
                                <th scope="col">@lang('User Phone')</th>
                                <th scope="col">@lang('Product')</th>
                                <th scope="col">@lang('BRO Package Qty')</th>
                                <th scope="col">@lang('Pick Up Date')</th>
                                <th scope="col">@lang('Status')</th>
                                <th scope="col">@lang('Product List')</th>
                                <th scope="col">@lang('Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($items as $ex)
                            <tr>
                                <td data-label="@lang('Date')">{{ date('Y-m-d',strtotime($ex->created_at)) }}</td>
                                <td data-label="@lang('Time')">{{ date('H:i:s',strtotime($ex->created_at)) }}</td>
                                {{-- <td data-label="@lang('SL')">{{ $items->firstItem()+$loop->index }}</td> --}}
                                {{-- <td data-label="@lang('Date')">{{ showDateTime($ex->created_at) }}</td> --}}
                                <td data-label="@lang('Delivery ID')">{{ $ex->trx }}</td>
                                {{-- <td data-label="@lang('User')">{{ $ex->user->username }}</td> --}}
                                <td data-label="@lang('User')"><a
                                        href="{{ route('admin.users.detail', $ex->user_id) }}">{{ $ex->username
                                        }}</a></td>
                                <td data-label="@lang('User Phone')">{{ $ex->mobile }}</td>
                                <td data-label="@lang('Product')">
                                    @if ($ex->product == 0)
                                        <span class="badge badge--warning">@lang('Product has not been selected')</span>
                                    @elseif($ex->product == 1)
                                        <button class="btn btn--primary btn-sm ml-1 detailBtnProd" data-id="{{ $ex->id }}"
                                            data-trx="{{ $ex->trx }}"
                                            data-username="{{ $ex->username }}"
                                            data-type="{{ $ex->pname ?? null }}"
                                            data-phone="{{ $ex->mobile }}"
                                            data-qty="{{ $ex->bro_qty }}"
                                            data-toggle="tooltip" data-original-title="@lang('Product Selected')"><i
                                            class="fas fa-info"></i>
                                        @lang('Product Selected')
                                        </button>
                                    @endif
                                </td>

                                <td data-label="@lang('BRO Package Qty')">{{ $ex->bro_qty }}</td>
                                <td data-label="@lang('Pick Up Date')">{{showDateTime($ex->pickupdate)}}</td>
                                <td data-label="@lang('Status')">
                                    @if ($ex->product == 0)
                                        <span class="badge badge--warning">@lang('Product has not been selected')</span>
                                    @else
                                        @if($ex->status == 1)
                                        <span class="badge badge--success">@lang('Complete. Already Pick Up')</span>
                                        @elseif($ex->status == 2)
                                        <span class="badge badge--warning">@lang('Waiting For Pick Up')</span>
                                        @elseif($ex->status == 3)
                                        <span class="badge badge--danger">@lang('Cancel')</span>
                                        @endif
                                        @if($ex->no_resi != null)
                                        <button class="btn--info btn-rounded  badge detailBtn"
                                            data-no_resi="{{$ex->no_resi}}"><i class="fa fa-info"></i></button>
                                        @endif
                                    @endif
                                </td>
                                <td data-label="@lang('Product List')">
                                    {{ $ex->productlist}}
                                </td>
                                <td>
                                    @if ($ex->product == 0)
                                        <form action="{{route('admin.resend.email',[$ex->user_id,$ex->trx])}}" method="post">
                                            @csrf
                                            <button type="submit" class="btn btn-sm btn-primary">
                                                <i class="fas fa-paper-plane"></i>Resend email</button>
                                        </form>
                                    @else
                                        @if ($ex->status == 2)

                                        <button class="btn btn--primary btn-sm ml-1 devBtn" data-id="{{ $ex->id }}"
                                            data-toggle="tooltip" data-original-title="@lang('Deliver')"><i
                                                class="fas fa-truck"></i>
                                            @lang('Deliver')
                                        </button>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                            </tr>
                            @endforelse

                        </tbody>
                    </table><!-- table end -->
                </div>
            </div>
            {{-- <div class="card-footer py-4">
                {{ paginateLinks($items) }}
            </div> --}}
        </div><!-- card end -->
    </div>
</div>


<div class="modal fade" id="exchange_verif" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Verification Exchange Request ID : <span
                        id="ex_id"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn--secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn--danger reject">Reject</button>
                <button type="button" class="btn btn--primary accept">Accept</button>
            </div>
        </div>
    </div>
</div>

<div id="detailModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Details')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="@lang('Close')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Pick Up By : <span class="withdraw-detail"></span></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn--danger" data-dismiss="modal">@lang('Close')</button>
            </div>
        </div>
    </div>
</div>
<div id="detailAlm" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Details')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="@lang('Close')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Alamat : <span class="withdraw-detail"></span>. <br> Kode Pos : <span
                        class="withdraw-detail1"></span>
                    <br> Nama Penerima : <span class="withdraw-detail3"></span>
                    <br> No Telp Penerima : <span class="withdraw-detail2"></span>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn--danger" data-dismiss="modal">@lang('Close')</button>
            </div>
        </div>
    </div>
</div>


<div id="deliverModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Confirm BRO Pack Pick Up')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('admin.BroDeliver.deliver')}}" method="POST">
                @csrf
                <input type="hidden" name="id">
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label class="font-weight-bold" for="">Pick Up by :</label>
                            <input class="form-control " type="text" name="no_resi" id="no_resi" placeholder="name"
                                required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn--dark" data-dismiss="modal">@lang('Close')</button>
                    <button type="submit" class="btn btn--success">@lang('Submit')</button>
                </div>
            </form>
        </div>
    </div>
</div>
@forelse($items as $ex)
<div id="detailprod{{$ex->id}}" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Details TRX : ')<span class="proddetail"></span></h5>
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="@lang('Close')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group mt-3">
                    <li class="list-group-item d-flex justify-content-between">
                        <span>@lang('Username ')</span> <span class="usrdetail font-weight-bold"></span>
                    </li>
                    <li class="list-group-item rounded-0 d-flex justify-content-between">
                        <span>@lang('BRO type')</span> <span class="typedetail font-weight-bold"></span>
                    </li>
                    <li class="list-group-item rounded-0 d-flex justify-content-between">
                        <span>@lang('BRO QTY')</span> <span class="qtydetail font-weight-bold"></span>
                    </li>
                    <li class="list-group-item rounded-0 d-flex justify-content-between">
                        <span>@lang('User Phone')</span> <span class="phonedetail font-weight-bold"></span>
                    </li>
                </ul>
                <br>
                <table class="table table--light style--two">
                    <thead>
                        <tr>
                            <th scope="col">@lang('Product')</th>
                            <th scope="col" width="20%">@lang('Qty')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse(brodetail($ex->id) as $exd)
                            <tr>
                                <td data-label="@lang('Product')">
                                    {{cekproduk($exd->product)}}
                                </td>
                                <td data-label="@lang('Qty')">
                                    {{$exd->q}}
                                </td>
                            </tr>
                        @empty
                        <tr>
                            <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endforeach

@endsection
@push('script')
<script>
    "use strict";
        (function ($) {
            $('.exchange_verif').on('click', function () {
                // console.log($(this).data('id'));
                var id = $(this).data('id');
                var modal = $('#exchange_verif');
                modal.find('#ex_id').html($(this).data('exid'));
                modal.find('.reject').on('click',function(){
                    document.getElementById("rform"+id).submit();
                    // console.log(id);
                });
                modal.find('.accept').on('click',function(){
                    document.getElementById("aform"+id).submit();
                    // console.log(id);
                });
        //         modal.find('#prices').val($(this).data('price'));
        //         modal.find('#price').html($(this).data('price'));
        //         modal.find('#weight').val($(this).data('weight'));
        //         modal.find('#tweight').html($(this).data('tweight'));
        //         modal.find('#product_id').val($(this).data('id'));
        //         modal.find('#product_name').val($(this).data('name'));

        //         modal.find('#qty').on('keyup change',function() { 
        // // alert('okl');
        //             modal.find('#total').val(modal.find('#qty').val() * modal.find('#weight').val().replace(/(\.\d{2})\d+/g, '$1'));
        //             modal.find('#total_rp').val(modal.find('#qty').val() * modal.find('#prices').val());
        //             modal.find('#totals').val(modal.find('#qty').val() * modal.find('#weight').val().replace(/(\.\d{2})\d+/g, '$1'));
        //         });
        //         // modal.find('.weight').val($(this).data('weight'));
        //         // var input = modal.find('.image');
        //         // // input.setAttribute("value", "http://localhost/microgold/assets/images/avatar.png");

        //         // if($(this).data('status')){
        //         //     modal.find('.toggle').removeClass('btn--danger off').addClass('btn--success');
        //         //     modal.find('input[name="status"]').prop('checked',true);

        //         // }else{
        //         //     modal.find('.toggle').addClass('btn--danger off').removeClass('btn--success');
        //         //     modal.find('input[name="status"]').prop('checked',false);
        //         // }

        //         // modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });

            // $('.modal').on('hidden.bs.modal', function(){
            //     $(this).find('form')[0].reset();
            // });

            // $('.add-product').on('click', function () {
            //     var modal = $('#add-product');
            //     modal.modal('show');
            // });

            $('.devBtn').on('click', function () {
                var modal = $('#deliverModal');
                modal.find('input[name=id]').val($(this).data('id'));
                modal.modal('show');
            });

            
        })(jQuery);

</script>
<script>
    $('.detailBtn').on('click', function () {
            var modal = $('#detailModal');
            var feedback = $(this).data('no_resi');
            modal.find('.withdraw-detail').html(feedback);
            modal.modal('show');
    });
</script>
<script>
    $('.detailAlm').on('click', function () {
            var modal = $('#detailAlm');
            var feedback = $(this).data('alamat');
            var pos = $(this).data('pos');
            var nhp = $(this).data('nhp');
            var np = $(this).data('np');
            modal.find('.withdraw-detail').html(feedback);
            modal.find('.withdraw-detail1').html(pos);
            modal.find('.withdraw-detail2').html(nhp);
            modal.find('.withdraw-detail3').html(np);
            modal.modal('show');
    });
</script>
<script src="{{ asset('assets/admin/js/vendor/datepicker.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/vendor/datepicker.en.js') }}"></script>
<script>
    'use strict';
    (function($){
        if(!$('.datepicker-here').val()){
            $('.datepicker-here').datepicker();
        }
    })(jQuery)
</script>
<script>
    $('.detailBtnProd').on('click', function () {
            var id = $(this).data('id');
            var trx = $(this).data('trx');
            var username = $(this).data('username');
            var type = $(this).data('type');
            var phone = $(this).data('phone');
            var qty = $(this).data('qty');
            var modal = $('#detailprod'+id);
            modal.find('.proddetail').html(trx);
            modal.find('.usrdetail').html(username);
            modal.find('.typedetail').html(type);
            modal.find('.phonedetail').html(phone);
            modal.find('.qtydetail').html(qty);
            modal.modal('show');
    });
</script>


{{-- <script src="{{ asset('assets/admin/js/vendor/datepicker.min.js') }}"></script> --}}
{{-- <script src="{{ asset('assets/admin/js/vendor/datepicker.en.js') }}"></script> --}}
<script>
    'use strict';
  (function($){
      if(!$('.datepicker-here').val()){
          $('.datepicker-here').datepicker();
      }
  })(jQuery)
</script>

  
<script src="https://cdn.datatables.net/1.13.5/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.1/js/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.2/moment.min.js"></script>
<script src="https://cdn.datatables.net/datetime/1.5.1/js/dataTables.dateTime.min.js"></script>
<script>
    $(document).ready( function () {
    // $('#myTable').DataTable({
    //     "pageLength": 25,
    //     order: [[0, 'desc']],
    //     dom: 'Bfrtip',
    //     buttons: [
    //         'copy', 'csv', 'excel'
    //     ]
    // });
} );
</script>



<script>
    
    let minDate, maxDate;
 
 // Custom filtering function which will search data in column four between two values
 DataTable.ext.search.push(function (settings, data, dataIndex) {
     let min = minDate.val();
     let max = maxDate.val();
     let date = new Date(data[0]);
  
     if (
         (min === null && max === null) ||
         (min === null && date <= max) ||
         (min <= date && max === null) ||
         (min <= date && date <= max)
     ) {
         return true;
     }
     return false;
 });
  
 // Create date inputs
 minDate = new DateTime('#min2', {
     format: 'YYYY-MM-DD'
 });
 maxDate = new DateTime('#max2', {
     format: 'YYYY-MM-DD'
 });
  
 // DataTables initialisation
 let table = $('#myTable').DataTable({
        "pageLength": 25,
        order: [[0, 'desc']],
        responsive: true,
        columnDefs: [{
            targets: [9], // Ganti dengan indeks kolom yang sesuai (dimulai dari 0)
            visible: false
            }],
        // sDom: "Bfrtip",
        // sDom: '<"top"Bf>rt<"bottom"ip><"clear">',
        sDom: "<'row'<'col-sm-3'B><'col-sm-6'<'toolbar row'>><'col-sm-3'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'lp>>",
                fnInitComplete: function(){
                    $('div.toolbar').html('<div class="col-md-3" style="font-size: 12px;line-height: 3;">Search By Date : </div> <div class="col-md-4"><input class="form-control"  type="date" id="min" name="min"></div><div class="col-md-4"><input class="form-control"  type="date" id="max" name="max"></div>');
                },
        buttons: [
            'copy', 'csv', 'excel'
        ]
    });
    // $('div.toolbar').html('');
    $('#min').change(function() {
        var date = $(this).val();
        $('#min2').val($(this).val());
        minDate.val($(this).val());
        // console.log(minDate.val());
    });
    $('#max').change(function() {
        var date = $(this).val();
        $('#max2').val($(this).val());
        maxDate.val($(this).val());
        // console.log(maxDate.val());
    });
  
 // Refilter the table
 document.querySelectorAll('#min2, #max2').forEach((el) => {
     el.addEventListener('change', () => table.draw());
 });

</script>
@endpush

@push('breadcrumb-plugins')
{{-- <div class="row">

    <div class="col-md-12 col-12">

        <form action="{{ route('admin.BroDelivery.search2') }}" method="GET"
            class="form-inline float-sm-right bg--white ml-2">
            <div class="input-group has_append">
                <input type="text" name="search" class="form-control" placeholder="@lang('Username/BRO/Delivery ID')"
                    value="{{ $search ?? '' }}">
                <div class="input-group-append">
                    <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>
        <form action="" method="GET" class="form-inline float-sm-right bg--white ml-2">
            <div class="input-group has_append">
                <input name="date" type="text" data-range="true" data-multiple-dates-separator=" - " data-language="en"
                    class="datepicker-here bg--white text--black form-control" data-position='bottom right'
                    placeholder="@lang('Min - Max pick up date')" autocomplete="off" value="{{ @$dateSearch }}"
                    readonly>
                <select name="status" id="status" class="from-select">
                    <option disabled {{ $sts=='' ? 'selected' : '' }}>Filter by Status</option>
                    <option value="2" {{ $sts==2 ? 'selected' : '' }}>Waiting for pick up
                    </option>
                    <option value="1" {{ $sts==1 ? 'selected' : '' }}>Complete. Already Pick Up
                    </option>
                </select>



                <div class="input-group-append">
                    <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-2 col-12">
        <form action="{{ route('admin.export.BROpickup') }}" method="GET" class="form-inline">
            <input hidden type="text" name="search" class="form-control" placeholder="@lang('Username or email')"
                value="{{ $search ?? '' }}">
            <input hidden type="text" name="date" class="form-control" placeholder="@lang('Username or email')"
                value="{{ $dateSearch ?? '' }}">
            <input hidden type="text" name="status" class="form-control" placeholder="@lang('Username or email')"
                value="{{ $sts ?? '' }}">
            <button class="btn btn--primary" type="submit">Export</button>
        </form>
    </div>
</div> --}}
@endpush

@push('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.5/css/jquery.dataTables.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/datetime/1.1.1/css/dataTables.dateTime.min.css">
@endpush